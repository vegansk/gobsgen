package gobsgen

import (
    "bytes"
    "io/ioutil"
    "os"
    "path/filepath"
    "reflect"
    "testing"
)

func TestParseDirective(t *testing.T) {
    normDir := "\tintlist   = list ( int , *[]string )  "
    dl := newDirLine(1, normDir)
    dir, err := parseDirective(dl, "test.go")

    t.Logf("Normal directive: %#v", dir)

    if err != nil || reflect.DeepEqual(dir, directive{"intlist", "list", []string{"int", "*[]string"}}) {
        t.Errorf("Error parsing directive \"%s\"", normDir)
        t.FailNow()
    }

    errDir := "intlist list int , *[]string)"
    dl = newDirLine(1, errDir)
    dir, err = parseDirective(dl, "test.go")

    t.Logf("Error in directive: %#v", err)

    if err == nil {
        t.Errorf("Error directive parsed, it's a bug!")
        t.FailNow()
    }
}

const source = `//GOT: templ/intlist = templ/list(int, *[]string)

//GOT: templ/bytelist = templ/list(byte)
`

func TestParseStream(t *testing.T) {
    stream := bytes.NewReader([]byte(source))
    dirs, err := parseStream(stream, "test.go")
    if err != nil {
        t.Errorf("Error: %v", err)
        t.FailNow()
    }

    expected := []*directive{
        &directive{"templ/intlist", "templ/list", []string{"int", "*[]string"}},
        &directive{"templ/bytelist", "templ/list", []string{"byte"}},
    }

    if !reflect.DeepEqual(dirs, expected) {
        t.Errorf("Unexpected result!")
    }
}

func mustCreateDir(dir string, t *testing.T) {
    err := os.MkdirAll(dir, 0770)
    if err != nil {
        t.Error(err)
        t.FailNow()
    }
}

func mustCreateTempDir(t *testing.T) string {
    res, err := ioutil.TempDir("", "gobsgen")
    if err != nil {
        t.Error(err)
        t.FailNow()
    }
    return res
}

func mustSaveToFile(file, data string, t *testing.T) {
    f, err := os.Create(file)
    if err != nil {
        t.Error(err)
        t.FailNow()
    }
    defer f.Close()

    _, err = f.Write([]byte(data))
    if err != nil {
        t.Error(err)
        t.FailNow()
    }
}

func mustReadFromFile(file string, t *testing.T) string {
    f, err := os.Open(file)
    if err != nil {
        t.Error(err)
    }
    defer f.Close()

    buff, err := ioutil.ReadAll(f)
    if err != nil {
        t.Error(err)
    }

    return string(buff)
}

func mustDeleteDir(dir string, t *testing.T) {
    if err := os.RemoveAll(dir); err != nil {
        t.Error(err)
    }
}

const listTemplate = `package list(type el)

type List struct {
    Data el
    Next *List
}
`

const goSource = `package main
//GOT: templ/intlist = list(int)
//GOT: templ/bytelist = list(byte)

import "templ/intlist"

func main() {
    x := intlist.List {100, nil}
}
`

const intlistFile = `package intlist

type List struct {
    Data int
    Next *List
}
`

const bytelistFile = `package bytelist

type List struct {
    Data byte
    Next *List
}
`

func TestGenerateFromSources(t *testing.T) {
    srcDir := mustCreateTempDir(t)
    defer mustDeleteDir(srcDir, t)
    t.Logf("Source dir = %s", srcDir)

    projDir := filepath.Join(srcDir, "project")
    mustCreateDir(projDir, t)

    templateFile := filepath.Join(projDir, "list.got")
    mustSaveToFile(templateFile, listTemplate, t)

    sourceFile := filepath.Join(projDir, "main.go")
    mustSaveToFile(sourceFile, goSource, t)

    if err := GenerateFromSources(srcDir, projDir); err != nil {
        t.Error(err)
    }

    result := mustReadFromFile(filepath.Join(srcDir, "templ", "intlist", "intlist.go"), t)
    if result != intlistFile {
        t.Errorf("Unexpected data:\n%s", result)
    }

    result = mustReadFromFile(filepath.Join(srcDir, "templ", "bytelist", "bytelist.go"), t)
    if result != bytelistFile {
        t.Errorf("Unexpected data:\n%s", result)
    }
}
