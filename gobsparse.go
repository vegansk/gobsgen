package gobsgen

import (
    b "bufio"
    "fmt"
    "io"
    u "io/ioutil"
    "os"
    p "path/filepath"
    s "strings"
)

const maxLinesToSkip = 10
const GOT_DIRECTIVE = "//GOT:"

type directive struct {
    newPackage string
    gotPackage string
    params     []string
}

func (d *directive) getTemplateFile(projectDir string) string {
    return p.Join(projectDir, s.Replace(d.gotPackage, "/", string(p.Separator), -1)+".got")
}

func (d *directive) getGeneratedFile(sourceDir string) string {
    tmp := s.Replace(d.newPackage, "/", string(p.Separator), -1)
    generatedFile := p.Base(tmp) + ".go"
    return p.Join(sourceDir, tmp, generatedFile)
}

func pathExists(path string) (bool, error) {
    _, err := os.Stat(path)
    if err == nil {
        return true, nil
    }
    if os.IsNotExist(err) {
        return false, nil
    }
    return false, err
}

// Check if generated package is out of date
func (d *directive) isOutOfDate(sourceDir, projectDir string) (res bool, err error) {
    templFile := d.getTemplateFile(projectDir)
    generatedFile := d.getGeneratedFile(sourceDir)

    generatedDir := p.Dir(generatedFile)

    exists, err := pathExists(generatedDir)
    if err != nil {
        return
    }
    if !exists {
        return true, nil
    }

    genInfo, err := os.Stat(generatedFile)
    if err != nil {
        if os.IsNotExist(err) {
            return true, nil
        }
        return
    }

    templInfo, err := os.Stat(templFile)
    if err != nil {
        return
    }

    if genInfo.ModTime().Unix() < templInfo.ModTime().Unix() {
        return true, nil
    }

    return false, nil
}

type directiveLine struct {
    lineNo int
    line   string
}

func newDirLine(lineNo int, line string) *directiveLine {
    return &directiveLine{lineNo, line}
}

// Parse sources in projectDir for GOT directives and generate
// packages from templates in sourceDir
func GenerateFromSources(sourceDir, projectDir string) (err error) {
    files, err := findGoFiles(projectDir)
    if err != nil {
        return
    }

    directives, err := parseFiles(files)
    if err != nil {
        return
    }

    for _, dir := range directives {
        if err = generateFromSource(dir, sourceDir, projectDir); err != nil {
            return err
        }
    }

    return nil
}

func generateFromSource(dir *directive, sourceDir, projectDir string) (err error) {
    outOfDate, err := dir.isOutOfDate(sourceDir, projectDir)
    if err != nil {
        return err
    }

    if !outOfDate {
        return
    }

    genFile := dir.getGeneratedFile(sourceDir)
    genDir := p.Dir(genFile)

    exists, err := pathExists(genDir)
    if err != nil {
        return err
    }
    if !exists {
        if err = os.MkdirAll(genDir, 0755); err != nil {
            return err
        }
    }

    fo, err := os.Create(genFile)
    if err != nil {
        return err
    }
    defer fo.Close()

    fi, err := os.Open(dir.getTemplateFile(projectDir))
    if err != nil {
        return err
    }
    defer fi.Close()

    params := Params{
        p.Base(dir.newPackage),
        dir.params,
    }

    err = GenerateFromFile(fi, fo, params)

    return
}

func findGoFiles(rootPath string) (files []string, err error) {
    fi, err := u.ReadDir(rootPath)
    if err != nil {
        return
    }

    for _, f := range fi {
        if f.IsDir() {
            subdir, err := findGoFiles(p.Join(rootPath, f.Name()))
            if err != nil {
                return nil, err
            }
            files = append(files, subdir...)
        } else if s.HasSuffix(f.Name(), ".go") {
            files = append(files, p.Join(rootPath, f.Name()))
        }
    }

    return
}

func parseFiles(fileNames []string) (directives []*directive, err error) {
    var f *os.File
    var dirs []*directive
    for _, fileName := range fileNames {
        f, err = os.Open(fileName)
        if err != nil {
            return
        }
        defer f.Close()

        dirs, err = parseStream(f, fileName)
        f.Close()
        if err != nil {
            return
        }

        directives = append(directives, dirs...)
    }
    return
}

func parseStream(is io.Reader, fileName string) (directives []*directive, err error) {
    scan := b.NewScanner(is)
    skipped := 0
    lineNo := 0

    var dirLines []*directiveLine
    for scan.Scan() {
        lineNo++
        line := s.TrimSpace(scan.Text())
        if !s.HasPrefix(line, GOT_DIRECTIVE) {
            skipped++
            if skipped >= maxLinesToSkip {
                break
            }
            continue
        }
        dirLines = append(dirLines, newDirLine(lineNo, s.TrimSpace(line[len(GOT_DIRECTIVE):])))
    }
    if err = scan.Err(); err != nil {
        return
    }

    for _, dirLine := range dirLines {
        if dir, err := parseDirective(dirLine, fileName); err == nil {
            directives = append(directives, dir)
            continue
        }
        return
    }

    return
}

// Parse string like this
//GOT: intlist=list(int,*[]string)
func parseDirective(dl *directiveLine, fileName string) (*directive, error) {
    spl := s.SplitN(dl.line, "=", 2)
    if len(spl) != 2 {
        return nil, fmt.Errorf("Error in GOT template directive, file \"%s\", line %d", fileName, dl.lineNo)
    }

    newPackage := s.TrimSpace(spl[0])
    packageDetail := s.TrimSpace(spl[1])

    spl = s.SplitN(packageDetail, "(", 2)
    if len(spl) != 2 {
        return nil, fmt.Errorf("Error in GOT template directive, file \"%s\", line %d", fileName, dl.lineNo)
    }

    gotPackage := s.TrimSpace(spl[0])

    if !s.HasSuffix(spl[1], ")") {
        return nil, fmt.Errorf("Error in GOT template directive, file \"%s\", line %d", fileName, dl.lineNo)
    }

    params := s.Split(spl[1][:len(spl[1])-1], ",")
    for idx, param := range params {
        params[idx] = s.TrimSpace(param)
    }
    return &directive{newPackage, gotPackage, params}, nil
}
