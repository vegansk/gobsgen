package gobsgen

import (
    "bytes"
    "go/scanner"
    "go/token"
    "testing"
)

func TestGetTokenFile(t *testing.T) {
    var buff []byte = []byte("This is a test buffer")
    t.Logf("Buff == %v, len is %d", buff, len(buff))
    var f *token.File
    if f = getTokenFile(buff, "<string>"); f == nil {
        t.Errorf("getTokenFile returns nil!")
    }
}

func TestGetTypes(t *testing.T) {
    code := "package test(type a, type b a, type c)"
    f := getTokenFile([]byte(code), "<code>")
    var s scanner.Scanner
    s.Init(f, []byte(code), nil, 0)
    tok := token.COMMA
    for tok != token.LPAREN && tok != token.EOF {
        _, tok, _ = s.Scan()
    }
    params, types, _, _ := getTypes(&s)
    t.Logf("Params = %v, Types = %v", params, types)
}

const templateCode = `package list(type item)

type List struct {
    Item item
    Next *List
}

func Cons(head item, tail *List) (res *List) {
    res = &List{head, tail}
}
`

const intCode = `package intlist

type List struct {
    Item int
    Next *List
}

func Cons(head int, tail *List) (res *List) {
    res = &List{head, tail}
}
`

func TestGenerateFromBuffer(t *testing.T) {
    codeBuff := []byte(templateCode)
    var out bytes.Buffer
    params := Params{"intlist", []string{"int"}}
    GenerateFromBuffer(codeBuff, "<code>", &out, params)
    result := out.String()
    t.Logf("Result:\n%s", result)
    if intCode != result {
        t.Error("Wrong generated code!")
    }
}
