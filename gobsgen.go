package gobsgen

import (
    "fmt"
    "go/scanner"
    "go/token"
    "io"
    "io/ioutil"
    "os"
    "strings"
)

type Scanner interface {
    Scan() (token.Pos, token.Token, string)
}

// Generator parameters
type Params struct {
    PackageName string   // Package name, using from template if empty
    Types       []string // Generic parameter types
}

// Generate generics implementation from given file,
// using given parameters and write it to the output stream.
func GenerateFromFile(fi *os.File, w io.Writer, params Params) (err error) {
    var input []byte
    if input, err = ioutil.ReadAll(fi); err != nil {
        return
    }

    err = GenerateFromBuffer(input, fi.Name(), w, params)

    return nil
}

// Generate generics implementation from given buffer,
// using given parameters and write it to the output stream.
func GenerateFromBuffer(buff []byte, fileName string, w io.Writer, params Params) (err error) {
    file := getTokenFile(buff, fileName)

    var scan scanner.Scanner
    //TODO: Use scanner.ErrorHandler for better diagnostic
    scan.Init(file, buff, nil, 0)

    tok := token.COMMA
    for tok != token.EOF && tok != token.PACKAGE {
        _, tok, _ = scan.Scan()
    }

    if tok == token.EOF {
        return fmt.Errorf("Unexpected EOF...")
    }

    _, tok, packageName := scan.Scan()
    if tok != token.IDENT {
        return fmt.Errorf("Expected package name but found '%s'", packageName)
    }

    if params.PackageName == "" {
        params.PackageName = packageName
    }

    //TODO: Understand - do we need some prefix/postfix for funcs

    if _, tok, lit := scan.Scan(); tok != token.LPAREN {
        return fmt.Errorf("Expected '(' but found '%s'", lit)
    }

    genericParams, genericTypes, restpos, e := getTypes(&scan)
    if e != nil {
        return
    }
    vartypes := make(map[string]string)
    imports := []string{}
    for i, t := range genericTypes {
        if i < len(params.Types) {
            if dot := strings.LastIndex(params.Types[i], "."); dot != -1 {
                // We've got to add an import!
                imports = append(imports,
                    "import "+genericParams[i]+` "`+params.Types[i][0:dot]+`"`)
                vartypes[genericParams[i]] = genericParams[i] + params.Types[i][dot:]
            } else {
                vartypes[genericParams[i]] = params.Types[i]
            }
        } else {
            if t == params.Types[0] {
                vartypes[params.Types[i]] = vartypes[params.Types[0]]
            } else {
                vartypes[params.Types[i]] = t
            }
        }
    }

    lastpos := int(restpos) + 1
    // Now let's write the package file...
    fmt.Fprintf(w, "package %s\n", params.PackageName)
    for _, imp := range imports {
        // These are extra imports for data types...
        fmt.Fprintf(w, "%s\n", imp)
    }

    pos, tok, lit := scan.Scan()
    for tok != token.EOF {
        if t, ok := vartypes[string(lit)]; ok {
            fmt.Fprint(w, string(buff[lastpos:int(pos)-1]))
            fmt.Fprint(w, t)
            lastpos = int(pos) - 1 + len(lit)
        }
        newpos, newtok, newlit := scan.Scan()
        if string(lit) == string(packageName) && newtok == token.PERIOD {
            fmt.Fprint(w, string(buff[lastpos:int(newpos)-1]))
            //TODO: Prefix!!!
            //fmt.Fprint(out, *prefix)
            lastpos = int(newpos) - 1
            pos, tok, lit = scan.Scan()
        } else {
            pos, tok, lit = newpos, newtok, newlit
        }
    }
    fmt.Fprintf(w, string(buff[lastpos:]))

    return
}

func getTokenFile(buff []byte, fileName string) *token.File {
    fs := token.NewFileSet()
    return fs.AddFile(fileName, fs.Base(), len(buff))
}

func getTypes(s Scanner) (params []string, types []string, pos token.Pos, err error) {
    tok := token.COMMA
    var lit string
    for tok == token.COMMA {
        pos, tok, lit = s.Scan()
        if tok != token.TYPE {
            err = fmt.Errorf("Expected 'type', not '%s'", lit)
            return
        }
        var tname string
        var par string
        pos, tok, par = s.Scan()
        if tok != token.IDENT {
            err = fmt.Errorf("Identifier expected, not '%s'", string(par))
            return
        }
        tname, pos, tok, lit = getType(s)
        params = append(params, string(par))
        types = append(types, string(tname))
    }
    if tok != token.RPAREN {
        err = fmt.Errorf("Inappropriate token %v with lit: %s",
            tok, lit)
    }
    return
}

func getType(s Scanner) (t string, pos token.Pos, tok token.Token, lit string) {
    pos, tok, lit = s.Scan()
    for tok != token.RPAREN && tok != token.COMMA {
        t += lit
        pos, tok, lit = s.Scan()
    }
    if t == "" {
        t = "interface{}"
    }
    return
}
